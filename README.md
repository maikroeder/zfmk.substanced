==================================================
BiNHum – Biodiversity Network of the Humboldt-Ring
==================================================

Access and format of existing digital object data, alignment of established database systems and development of a
joint data portal BiNHum is a joint project of five natural history museums and research collections representing
the Humboldt-Ring. These are the State Museums of Natural History Karlsruhe (SMNK) and Stuttgart (SMNS), the
Zoological Research Museum Alexander Koenig in Bonn (ZFMK), the Bavarian Natural History Collections in Munich
(SNSB), and the Botanic Garden and Botanical Museum Berlin-Dahlem (BGBM).

The three-year project (2012–2015) will enforce collaboration and consolidate collection data of the Humboldt-Ring
institutes and their associates by development of a joint data portal and efforts in data mining, standardisation,
new data types as 3D images e.g., or data digitized by the sub-project MORPHYLL at the SMNS (DFG support code
RO 3250/21, acquisition of ecophysiologically relevant morphometric data of fossil leaves), and data porting to
current IT standards. Additionally, the portal will provide all accessible data to international projects as GBIF.

The portal provides a plattform to search and display the previously harvested collection data of the project partners.
Additionally a widget (also calls "spoke") is available to integrate the portal search into external web sites.

See [project wiki page] (http://wiki.binhum.net/web/) for detailed information.

Setup
-----
The main components of the project are
* MySQL database to store data harvested by the B-HIT tool (http://wiki.bgbm.org/bhit/index.php/Main_Page)
* Apache Solr installation to index data (git@git.morphdbase.de:christian/BiNHum_Solr.git)
* Python based portal (this repository)

Git repository:
---------------
Find the latest version at
http://git.morphdbase.de/christian/BiNHum_Solr.git

Install:
--------

```
mkdir substanced-binhum
cd substanced-binhum
git clone https://github.com/maikroeder/substanced.git
virtualenv -p python2.7 --no-site-packages env
cd substanced
../env/bin/python setup.py dev
cd ../env
git clone git@bitbucket.org:maikroeder/zfmk.substanced.git
cd zfmk.substanced
make build
```

Start serve
-----------
Specify the port for the application at *development.ini* or *nodebug.ini* (here e.g. 17001) and start application:

```
    # in debug mode
    ../bin/pserve development.ini

    # in production mode
    ../bin/pserve nodebug.ini
```

Functional Tests
----------------

```
    ../bin/pip install  beautifulsoup4
    ../bin/python ftests/ftests.py
```

Configuration
-------------
**Webserver:**
It is advised to proxy the python application trough a webserver (e.g. Apache).
```
ProxyPass / http://localhost:17001/
ProxyPassReverse / http://localhost:17001/
ProxyPreserveHost On
```

**General Setup:**
http://www.[YOUR_DOMAIN].net/manage/portal/binhum/@@properties
Portal title, etc.

**Solr:**
Login at http://www.[YOUR_DOMAIN].net/manage/database/solr/@@properties to configure the solr properties of your
instance:
* URL: Domain + path of your solr core (e.g. http://solr.[YOUR_DOMAIN].net/solr/collection1/
* Fields: List for index fields of your Solr instance

**Translation:**
http://www.[YOUR_DOMAIN].net/manage/de/@@contents and http://www.[YOUR_DOMAIN].net/manage/en/@@contents to configure
German and English standard texts (home page, info, about, ...)

**GUI fields:**
http://www.[YOUR_DOMAIN].net/manage/field_display/binhum/@@properties
Configure, which fields to display in search results, filters, tables, facets and lables.

Start local Solr server
-----------------------
See Solr repository for details: 

```
cd [PATH_TO_SOLR] &&
java  -Xms128M -Xmx512M -jar start.jar
```

Browse
------
[http://www.[YOUR_DOMAIN].net](http://www.[YOUR_DOMAIN].net)

Administration Widget (= Spokes):
---------------------------------
An example of a widget
http://www.binhum.net/spoke/bgbm/

Screenshot of a spoke admin panel
http://my.jetscreenshot.com/18287/20140806-2lxe-87kb.jpg

The following parameters can be confugured:

**Name:** The name of the widget/url paht (admin only)

**Titel:** Used as Title of the widget

**Institutslogo:** Optional link to an exterrnal logo

**Instituts-URL:** Optional URL to institute

**Treffer pro Seite:** number of hits per page

**CSS-Datei:** URL to an external css file to adapt layout

**Institutscode:** Institution code used in the date sets (like zfmk, bgbm, ...) to pre-filter data of the widget 

**Fortgeschrittene Optionen:** Advanced solr parameters for pre-filtering (e.g. year, type, country, ...). Optional 

**Example:**
http://my.jetscreenshot.com/18287/20140806-pfkr-189kb.jpg

**example iframe:**
```
<iframe width="600" src="http://www.binhum.net/spoke/bgbm/"></iframe>
```

Currently 5 widgets exist:

**Widget:**
http://demo.binhum.net/spoke/bgbm/
http://demo.binhum.net/spoke/smns/
http://demo.binhum.net/spoke/snafu/
http://demo.binhum.net/spoke/zfmk/
http://demo.binhum.net/spoke/m/

**Admin-links:**
http://www.binhum.net//manage/principals/users/spokeadmin/bgbm/@@properties
http://www.binhum.net//manage/principals/users/spokeadmin/smns/@@properties
http://www.binhum.net//manage/principals/users/spokeadmin/snafu/@@properties
http://www.binhum.net//manage/principals/users/spokeadmin/zfmk/@@properties
http://www.binhum.net//manage/principals/users/spokeadmin/m/@@properties

**Loging** (please change after 1st login):
Benutzer: spokeadmin
Passwort: bh2014admin

