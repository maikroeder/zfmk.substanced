# -*- coding: utf-8 -*-
import codecs
import atexit
import requests
import random
import string
from bs4 import BeautifulSoup

SITE = "http://localhost:6543"
USERID = 'editor'
PASSWORD = 'editor'


# Play nice and close the database connection when exiting
def debug():
    import os
    output_file = open("debug.html", 'r')
    if output_file.read() == "":
        print "empty debug.html file"
    else:
        os.system("open debug.html")
    output_file.close()


def write_debug_file(request):
    debugfile = codecs.open('debug.html', "w", "utf-8")
    debugfile.write(request.text)
    debugfile.close()

atexit.register(debug)


def test_1():
    print "Access the Home Page"
    request = requests.get(SITE + '/')
    write_debug_file(request)
    assert request.headers['server'] == 'waitress'
    assert 'date' in request.headers
    assert 'content-length' in request.headers
    assert request.headers['server'] == 'waitress'
    assert request.headers['content-type'] == 'text/html; charset=UTF-8'
    assert 'BiNHum' in request.text
    assert 'Search' in request.text


def test_2():
    print "Direct access to Dashboard Page fails for Anonymous"
    request = requests.get(SITE + '/dashboard')
    write_debug_file(request)
    assert request.headers['server'] == 'waitress'
    assert 'date' in request.headers
    assert 'content-length' in request.headers
    assert request.headers['server'] == 'waitress'
    assert request.headers['content-type'] == 'text/html; charset=UTF-8'
    assert 'Log In' in request.text


def test_3():
    print "Login Page"
    request = requests.get(SITE + '/login')
    write_debug_file(request)
    soup = BeautifulSoup(request.text)
    for input_tag in soup.find_all('input'):
        if input_tag.get('name') == "csrf_token":
            csrf_token = input_tag.get('value')
    cookies = dict(set(request.cookies.items()))

    print "Login"
    payload = {'login': USERID, 'password': PASSWORD, 'form.submitted': 'Log In', 'csrf_token': csrf_token}
    request = requests.post(SITE + '/login', data=payload, cookies=cookies, headers=dict(Referer=SITE + '/login'))
    write_debug_file(request)

    # Check userid in cookie
    cookies = dict(set(request.cookies.items()))
    print cookies
    assert request.headers['server'] == 'waitress'
    assert 'date' in request.headers
    assert 'content-length' in request.headers
    assert request.headers['server'] == 'waitress'
    assert request.headers['content-type'] == 'text/html; charset=UTF-8'
    assert 'BiNHum-Portal' in request.text


def test_4():
    print "Signup Page"
    request = requests.get(SITE + '/signup')
    write_debug_file(request)
    soup = BeautifulSoup(request.text)
    for input_tag in soup.find_all('input'):
        if input_tag.get('name') == "csrf_token":
            csrf_token = input_tag.get('value')
    cookies = dict(set(request.cookies.items()))

    name = ''.join(random.choice(string.ascii_lowercase + string.digits) for x in range(16))
    email = name + '@example.com'
    print "Signup"
    payload = {'name': name, 'email': email, 'password': PASSWORD, 'form.submitted': 'Log In', 'csrf_token': csrf_token}
    request = requests.post(SITE + '/signup', data=payload, cookies=cookies, headers=dict(Referer=SITE + '/signup'))
    write_debug_file(request)

    # Check userid in cookie
    cookies = dict(set(request.cookies.items()))
    assert request.headers['server'] == 'waitress'
    assert 'date' in request.headers
    assert 'content-length' in request.headers
    assert request.headers['server'] == 'waitress'
    assert request.headers['content-type'] == 'text/html; charset=UTF-8'
    assert 'BiNHum-Portal' in request.text


def test_5():
    print "Check annotation button"
    request = requests.get(SITE + '/?tab=annotation&context=specimen&fq=id%3A%22BGBM/Lichen%20Herbarium%20Berlin/121529%22')
    write_debug_file(request)
    assert request.headers['server'] == 'waitress'
    assert 'date' in request.headers
    assert 'content-length' in request.headers
    assert request.headers['server'] == 'waitress'
    assert request.headers['content-type'] == 'text/html; charset=UTF-8'
    assert 'Add Annotation' in request.text


def test_6():
    print "Wrong geo location"
    request = requests.get(SITE + '?tab=map&geohash=sspmuxubkzu')
    write_debug_file(request)
    assert request.headers['server'] == 'waitress'
    assert 'date' in request.headers
    assert 'content-length' in request.headers
    assert request.headers['server'] == 'waitress'
    assert request.headers['content-type'] == 'text/html; charset=UTF-8'
    assert not "resultsin" in request.text


def test_7():
    print "Working?"
    request = requests.get(SITE + '?advanced_search_term=A.+J.+A.+Bonpland+%26+F.+W.+H.+A.+von+Humboldt&advanced_search=collectorname')
    write_debug_file(request)
    assert request.headers['server'] == 'waitress'
    assert 'date' in request.headers
    assert 'content-length' in request.headers
    assert request.headers['server'] == 'waitress'
    assert request.headers['content-type'] == 'text/html; charset=UTF-8'
    assert 'Landing Page' in request.text


def test_8():
    print "Working?"
    request = requests.get(SITE + '?tab=dashboard&fq=collectorname:%22A.%20J.%20A.%20Bonpland%20&%20F.%20W.%20H.%20A.%20von%20Humboldt%22')
    write_debug_file(request)
    assert request.headers['server'] == 'waitress'
    assert 'date' in request.headers
    assert 'content-length' in request.headers
    assert request.headers['server'] == 'waitress'
    assert request.headers['content-type'] == 'text/html; charset=UTF-8'
    assert 'Landing Page' in request.text


def test_9():
    print "Exclude does not just contain tab and context when removing id"
    request = requests.get(SITE + '?tab=info&context=specimen&fq=id:%22BGBM/Lichen%20Herbarium%20Berlin/121529%22')
    write_debug_file(request)
    assert request.headers['server'] == 'waitress'
    assert 'date' in request.headers
    assert 'content-length' in request.headers
    assert request.headers['server'] == 'waitress'
    assert request.headers['content-type'] == 'text/html; charset=UTF-8'
    assert 'Landing Page' in request.text


def test_10():
    print "JSON Error AnnoSys"
    request = requests.get(SITE + '/spoke/zfmk?tab=annotation&context=specimen&fq=id%3A%22ZFMK/COL/COL_2008_439%22')
    write_debug_file(request)
    assert request.headers['server'] == 'waitress'
    assert 'date' in request.headers
    assert 'content-length' in request.headers
    assert request.headers['server'] == 'waitress'
    assert request.headers['content-type'] == 'text/html; charset=UTF-8'
    assert 'Landing Page' in request.text


def test_11():
    print "Geohash should show point when only one result"
    request = requests.get(SITE + '?geohash=u0v27xx1g9h&tab=map&fq=isocountrycode%3A%22DE%22')
    write_debug_file(request)
    assert request.headers['server'] == 'waitress'
    assert 'date' in request.headers
    assert 'content-length' in request.headers
    assert request.headers['server'] == 'waitress'
    assert request.headers['content-type'] == 'text/html; charset=UTF-8'
    assert 'Landing Page' in request.text


if __name__ == '__main__':
    test_1()
    test_2()
    test_3()
    test_4()
    test_5()
    test_6()
    test_7()
    test_8()
    test_9()
    test_10()
    test_11()
