.PHONY: build

build:	
	if [ -d ../like.solr ]; then cd ../like.solr; git pull; fi
	if [ ! -d ../like.solr ]; then git clone ssh://git@bitbucket.org/maikroeder/like.solr.git ../like.solr; fi
	../bin/pip install -U ../like.solr

	../bin/python setup.py develop
